import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Login',
    name: 'Login',
    component: () => import('@bit/satryacode.dashboard.component-login/Login.vue')
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});
router.beforeEach((to, from, next) => {
  if (to.path === "/"){
    try {
      let authUser = JSON.parse(sessionStorage.getItem('authUser'));
      let token = authUser["token"];
      if(token){
        next(); console.log("to Home");
      }
    } catch (error) {
      console.log("Please login first")
      window.location.replace("/Login");
    }
  }
  next(); //this is needed
});

export default router
