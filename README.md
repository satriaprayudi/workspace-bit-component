# bit-vue-tutorial

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

# Bit

## Add component

```
bit add src/components/{component_name}/{component_name}.vue --id {component_name}
bit add src/components/{component_name}/{component_name}.js --id {component_name}
...
```

## Build component

```
bit build
```

## Tag component

```
bit tag --all 0.0.1
```

## Export component

```
bit export <username>.<scope>
```

## Import component to other project

```
bit import @bit/<username>.<scope>/{component_name}

<template>
    < {component_name} />
</template>

<script>
import {component_name} from '@bit/<username>.<scope>.{component_name}/{component_name}.vue';
export default {
    components: {
        {component_name}
    }
}
</script>

```

# About Component

## Shared Login Info

```
let data = sessionStorage.getItem('authUser');
```
